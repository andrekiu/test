import React from "react";

import { Button as AragonButton } from "@aragon/ui";

export default ({ loading, ...props }) => <AragonButton {...props} />;
