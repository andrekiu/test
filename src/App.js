import React from "react";
import Web3 from "web3";
import logo from "./logo.svg";
import { BrowserRouter as Router, Route } from "react-router-dom";
//import "./App.css";
import {
  AppBar,
  AppView,
  Table,
  TableHeader,
  TableRow,
  TableCell,
  Text,
  Button
} from "@aragon/ui";

import Blocks from "./Blocks";
import Transactions from "./Transactions";

class App extends React.Component {
  async componentDidMount() {}
  render() {
    return (
      <AppView appBar={<AppBar title="My App" />}>
        <Router>
          <Route path="/" exact component={Blocks} />
          <Route path="/:hash" exact component={Transactions} />
        </Router>
      </AppView>
    );
  }
}

export default App;
