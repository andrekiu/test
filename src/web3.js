import Web3 from "web3";

let web3 = window.web3;

const setUp = () => {
  if (typeof web3 !== "undefined") {
    web3 = new Web3(web3.currentProvider);
  } else {
    alert("install MetaMask");
  }
};

const gg = {
  setUp,
  getBlockNumber: () => web3.eth.getBlockNumber(),
  getBlock: v => web3.eth.getBlock(v),
  getTransaction: v => web3.eth.getTransaction(v)
};

export default gg;
