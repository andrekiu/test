import React, { useRef, useState, useEffect, useContext } from "react";
import Q from "q";
import web3 from "../web3";

const getRange = (array, start, size) => {
  const res = [];
  for (let i = 0; i < size && start + i < array.length; i++) {
    res.push(array[i + start]);
  }
  return res;
};

const hasEther = tx => +tx.value;

const useEtherTransactions = ({ blockNumber, pageSize = 5 } = {}) => {
  const [ids, setIds] = useState();
  const [current, setCurrent] = useState(0);
  const [fetching, setFetching] = useState(true);
  const [transactions, setTransactions] = useState([]);

  const refCurrent = useRef(0);
  useEffect(() => {
    refCurrent.current = current;
  });

  const hasMore = ids && current < ids.length;

  const fetchMore = async () => {
    setFetching(true);
    const results = await Q.all(
      getRange(ids, refCurrent.current, pageSize).map(web3.getTransaction)
    );
    const etherTransactions = results.filter(hasEther);
    setCurrent(current => current + pageSize);
    if (etherTransactions.length === 0 && hasMore) {
      // no funciona porq tiene el valot antiguo del current, con useRef lo soluciono
      return await fetchMore();
    }
    /*
			//ESTO no funciona por el useRef :(
    setFetching(true);
    const results = await Q.all(
      getRange(ids, current, pageSize).map(web3.getTransaction)
    );
    const etherTransactions = results.filter(hasEther);
    setCurrent(current => current + pageSize);
    if (etherTransactions.length === 0 && hasMore) {
      // no funciona porq tiene el valot antiguo del current, con useRef lo soluciono
      return await fetchMore();
    }
		*/

    setFetching(false);
    setTransactions(transactions => transactions.concat(etherTransactions));
  };

  useEffect(() => {
    const fetchBlock = async () => {
      const { transactions } = await web3.getBlock(blockNumber);
      setIds(transactions);
    };
    fetchBlock();
  }, [blockNumber]);

  useEffect(() => {
    if (ids) fetchMore();
  }, [ids]);

  return {
    transtactionsFetches: current,
    hasMore,
    fetchMore,
    fetching,
    transactions
  };
};

export default useEtherTransactions;
